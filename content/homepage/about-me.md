---
title: "About Us"
weight: 1
header_menu: true
---

![About Us](images/about-us.jpg)

At Sri Ramana Opticals and Computerized Eye Clinic, we understand the importance of maintaining good eye health, which is why we offer a wide range of services to cater to all your eye care needs. 

Whether you require Eye Testing, Glaucoma or Cataract screening, or need assistance with spectacle frames, lenses, sunglasses, or contact lenses, our experienced optometrists and opticians are dedicated to providing personalized care and expert advice to help you achieve optimal eye health.

Our collection of high-quality eyewear includes a wide range of spectacle frames, lenses, and sunglasses to suit every style and budget. 

So why wait? Visit us today and experience the best eye care with us. Our team is committed to helping you achieve clear and comfortable vision so that you can enjoy life to the fullest.